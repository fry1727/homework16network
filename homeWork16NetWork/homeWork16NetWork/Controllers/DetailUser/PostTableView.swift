//
//  PostTableView.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 26.02.21.
//

import UIKit

class PostTableView: UITableViewController {


    var posts: [Post] = []
    var userId: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath)

        let post = posts[indexPath.row]
        cell.textLabel?.text = post.title
        cell.detailTextLabel?.text = post.body
        cell.detailTextLabel?.numberOfLines = 0
        cell.textLabel?.numberOfLines = 0

        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPax = tableView.indexPathForSelectedRow {
            let commentVC = segue.destination as! CommentTableView
            if let id = posts[indexPax.row].id {
                commentVC.postId = id
            }
            let addPostVC = segue.destination as! addPostVC
            if let id = posts[indexPax.row].id {
                addPostVC.userId = id
            }
        }
    }
    
    
    private func fetchData() {
        let userUrl = "https://jsonplaceholder.typicode.com/posts?userId=\(userId)"
        guard let url = URL(string: userUrl) else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
            if let error = error {
                print("There is some error: \(error) with responce \(String(describing: responce))")
                return
            }
            if let responce = responce {
                print(responce)
            }
            guard let data = data else { return }
            do {
                self.posts = try JSONDecoder().decode([Post].self, from: data)
                //print(self.posts)
            } catch let error {
                print(error)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        task.resume()
    }

}
