//
//  addPostVC.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 2.03.21.
//

import UIKit
import Alamofire

class addPostVC: UIViewController {
    
    //var user: User = User(id: 0, name: "", username: "", email: "", phone: "", website: "", address: Address(street: "", suite: "", city: "", zipcode: "", geo: Geo(lat: "", lng: "")), company: Company(name: "", catchPhrase: "", bs: ""))
    var userId :Int?
    let url = URL(string: "https://jsonplaceholder.typicode.com/posts")

    @IBOutlet weak var titlePostTF: UITextField!
    @IBOutlet weak var textPostTf: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func sendPostClassic(_ sender: Any) {
        sendPostClassic()
    }
    

    @IBAction func sendPostAF(_ sender: Any) {
        sendPostAF()
    }
    
    private func sendPostClassic() {
        if let userId = userId,
            let url = url,
            let title = titlePostTF.text,
            let text = textPostTf.text {

            var request = URLRequest(url: url)

            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")

            let post: [String: Any] = ["userId": userId,
                                       "title": title,
                                       "body": text]

            guard let httpBody = try? JSONSerialization.data(withJSONObject: post, options: []) else { return }
            request.httpBody = httpBody

            URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
                print(response ?? "")
                if let _ = data {
                    DispatchQueue.main.async {
                        self?.navigationController?.popViewController(animated: true)
                    }
                } else if let error = error {
                    print(error)
                }
            }.resume()
        }
    }
    
    
    private func sendPostAF() {
        if let userId = userId,
            let url = url,
            let title = titlePostTF.text,
            let text = textPostTf.text {

            let post: [String: Any] = ["userId": userId,
                                       "title": title,
                                       "body": text]

            AF.request(url, method: .post, parameters: post, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                case .success(let data):
                    print(data)
                    self.navigationController?.popViewController(animated: true)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
}
