//
//  UserDetailView.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 26.02.21.
//

import UIKit

class UserDetailView: UIViewController {

    let postUrl = "https://jsonplaceholder.typicode.com/posts"


    var user: User = User(id: 0, name: "", username: "", email: "", phone: "", website: "", address: nil, company: nil)


    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!

    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var suiteLabel: UILabel!
    @IBOutlet weak var cityLAbel: UILabel!
    @IBOutlet weak var zipcodeLabel: UILabel!

    @IBOutlet weak var catchPhraseLAbel: UILabel!
    @IBOutlet weak var companyNameLAbel: UILabel!
    @IBOutlet weak var bsLabel: UILabel!
    @IBOutlet weak var postBtnLabel: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLabels()
    }

    @IBAction func postBtnAction(_ sender: Any) {
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let postsVC = segue.destination as? PostTableView {
            if let userId = user.id {
                postsVC.userId = userId
            }
        }
        if let albumsVC = segue.destination as? AlbumsTVC {
            if let userId = user.id {
                albumsVC.userID = userId
            }
        }
    }

    private func setupLabels() {
        nameLabel.text = user.name
        userNameLabel.text = user.username
        emailLabel.text = user.email
        phoneLabel.text = user.phone
        websiteLabel.text = user.website
        streetLabel.text = user.address?.street
        suiteLabel.text = user.address?.suite
        cityLAbel.text = user.address?.city
        zipcodeLabel.text = user.address?.zipcode
        catchPhraseLAbel.text = user.company?.catchPhrase
        bsLabel.text = user.company?.bs
        companyNameLAbel.text = user.company?.name
    }
}
