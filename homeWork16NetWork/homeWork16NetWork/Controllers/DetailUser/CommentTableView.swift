//
//  CommentTableView.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 28.02.21.
//

import UIKit

class CommentTableView: UITableViewController {

    var comments: [Comment] = []
    var postId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath)
        cell.textLabel?.text = comments[indexPath.row].name
        cell.detailTextLabel?.text = comments[indexPath.row].body
        cell.textLabel?.numberOfLines = 0
        cell.detailTextLabel?.numberOfLines = 0
        return cell
    }

    private func fetchData() {
        let userUrl = "https://jsonplaceholder.typicode.com/comments?postId=\(postId)"
        guard let url = URL(string: userUrl) else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
            if let error = error {
                print("There is some error: \(error) with responce \(String(describing: responce))")
                return
            }
            if let responce = responce {
                print(responce)
            }
            guard let data = data else { return }
            do {
                self.comments = try JSONDecoder().decode([Comment].self, from: data)
                print(self.comments)
            } catch let error {
                print(error)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        task.resume()
    }
}
