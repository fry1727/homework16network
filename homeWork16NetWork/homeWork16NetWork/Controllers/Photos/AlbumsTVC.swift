//
//  AlbumsTVC.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 2.03.21.
//

import UIKit
import Alamofire
import SwiftyJSON

class AlbumsTVC: UITableViewController {
    
    var userID: Int?
    var albums : [Album] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        getAlbums()

    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath)
        cell.textLabel?.text = String(albums[indexPath.row].id)
        cell.detailTextLabel?.text = albums[indexPath.row].title
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPax = tableView.indexPathForSelectedRow {
            let albumVC = segue.destination as! AlbumCVC
            albumVC.albumId = albums[indexPax.row].id
            
        }
    }
        
    private func getAlbums() {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/albums?userId=\(userID ?? 0)") else {return}
        
        AF.request(url).responseJSON { response in
            switch response.result {
            case .success(let data):
                guard  let jsonArray = data as? Array<[String : Any]> else { return }
                for jsonObj in jsonArray {
                guard
                    let userId = jsonObj["userId"] as? Int,
                    let title = jsonObj["title"] as? String,
                    let id = jsonObj["id"] as? Int
                 else { return
                 }
                    let album = Album(userId: userId, id: id, title: title)
                    self.albums.append(album)
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }

}
