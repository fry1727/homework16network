//
//  AlbumCVC.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 3.03.21.
//

import UIKit
import Alamofire

private let reuseIdentifier = "Cell"

class AlbumCVC: UICollectionViewController {
    
    var albumId : Int = 0
    var photos : [Photo] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        getAlbum()

    }

    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumViewCell", for: indexPath) as! AlbumViewCell
        //cell.imageView = UIImage()
        cell.backgroundColor = .red
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    
    private func getAlbum() {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/photos?albumId=\(albumId)") else {return}
        
        AF.request(url).responseJSON { response in
            switch response.result {
            case .success(let data):
                guard  let jsonArray = data as? Array<[String : Any]> else { return }
                for jsonObj in jsonArray {
                guard
                    let albumId = jsonObj["albumId"] as? Int,
                    let id = jsonObj["id"] as? Int,
                    let title = jsonObj["title"] as? String,
                    let url = jsonObj["url"] as? String,
                    let thumbnailUrl = jsonObj["thumbnailUrl"] as? String
                 else {
                    return
                 }
                    let photo = Photo(albumId: albumId, id: id, title: title, url: url, thumbnailUrl: thumbnailUrl)
                    self.photos.append(photo)
                    self.collectionView.reloadData()
                    
                    
                    print(self.photos)
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
}

extension AlbumCVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 20, height: 50)
    }
}
