//
//  UsersTableView.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 25.02.21.
//

import UIKit

class UsersTableView: UITableViewController {

    private let userUrl = "https://jsonplaceholder.typicode.com/users"

    private var users: [User] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserCell
        let user = users[indexPath.row]
        cell.configure(with: user)
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            let detailVC = segue.destination as! UserDetailView
            detailVC.user = users[indexPath.row]
        }
    }

    func fetchData() {
        guard let url = URL(string: userUrl) else { return }

        let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
            if let error = error {
                print("There is some error: \(error) with responce \(String(describing: responce))")
                return
            }
            if let responce = responce {
                print(responce)
            }
            guard let data = data else { return }
            do {
                self.users = try JSONDecoder().decode([User].self, from: data)
                print(self.users)
            } catch let error {
                print(error)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        task.resume()
    }
}
