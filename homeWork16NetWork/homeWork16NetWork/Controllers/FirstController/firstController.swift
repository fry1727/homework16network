//
//  firstController.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 24.02.21.
//

import UIKit

enum Actions: String, CaseIterable /*позволяет из перечисления сделать массив*/ {
    case downloadImage = "Download Image"
    case users = "Users"
}

class firstController: UICollectionViewController {
    
    private let actions = Actions.allCases
    
    // MARK: - UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actions.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActionCell", for: indexPath) as! ActionCell
        cell.cellLabel.text = actions[indexPath.item].rawValue
        return cell
    }
    
    // MARK: - UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let userAction = actions[indexPath.item]

        switch userAction {
        case .downloadImage:
            performSegue(withIdentifier: "ShowImage", sender: self)
        case .users:
            performSegue(withIdentifier: "Users", sender: self)
        }
    }
}

extension firstController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 50, height: 200)
    }
}
