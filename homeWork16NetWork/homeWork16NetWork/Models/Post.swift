//
//  Post.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 26.02.21.
//

import Foundation


struct Post : Decodable{
    let userId : Int?
    let id : Int?
    let title : String?
    let body : String?
}

