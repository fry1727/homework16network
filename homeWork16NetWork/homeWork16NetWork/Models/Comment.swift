//
//  Comment.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 28.02.21.
//

import Foundation


struct Comment: Decodable {
    let postId : Int?
    let id : Int?
    let name : String?
    let email : String?
    let body : String?
}



