//
//  Album.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 2.03.21.
//

import Foundation


struct Album : Decodable {
    let userId: Int
    let id : Int
    let title : String
}
