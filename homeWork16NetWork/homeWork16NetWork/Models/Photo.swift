//
//  Photo.swift
//  homeWork16NetWork
//
//  Created by Yauheni Skiruk on 3.03.21.
//

import Foundation


struct Photo {
    let albumId : Int?
    let id : Int?
    let title : String?
    let url : String?
    let thumbnailUrl : String?
}
 
